call plug#begin()
    Plug 'mattn/emmet-vim'
    Plug 'tpope/vim-surround'
    Plug 'leafgarland/typescript-vim'
    Plug 'othree/yajs.vim'
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
    Plug 'junegunn/fzf.vim'
    Plug 'voldikss/vim-floaterm'
    Plug 'diepm/vim-rest-console'
    Plug 'sonph/onehalf', { 'rtp': 'vim' }
    Plug 'peitalin/vim-jsx-typescript'
    Plug 'easymotion/vim-easymotion'
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()

let mapleader = " "

set number
set hlsearch
set tabstop=4
set expandtab
set shiftwidth=4

packloadall
packadd! dracula
syntax enable
set nocompatible

noremap  <leader>\ :nohl<CR>
imap jj <esc>

noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>

imap <up> <nop>
imap <down> <nop>
imap <left> <nop>
imap <right> <nop>

vnoremap < <gv
vnoremap > >gv

nmap <leader>e <Cmd>CocCommand explorer<CR>

vmap <leader>y "+y

nmap <c-p> :Files<cr>
nmap <c-S-p> <plug><fzf-complete-file)
nmap <leader>h <c-w>h
nmap <leader>l <c-w>l
nmap <leader>j <c-w>j
nmap <leader>k <c-w>k
nmap <C-a> ggVG

let g:floaterm_keymap_new    = '<F7>'
let g:floaterm_keymap_prev   = '<F8>'
let g:floaterm_keymap_next   = '<F9>'
let g:floaterm_keymap_toggle = '<F12>'
let g:floaterm_keymap_new = '<Leader>ft'

" set t_Co=256
set cursorline
" colorscheme onehalflight
let g:airline_theme='dracula'
" lightline
let g:lightline = { 'colorscheme': 'dracula' }
" colorscheme dracula
colorscheme monokai
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)
runtime ./coc.vim
runtime ./copy.vim

set ttimeoutlen=10
hi Visual term=reverse cterm=reverse guibg=Grey
